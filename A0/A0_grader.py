import A0 as A0

# When run at command line as "python A0_grader", should print 
# "hello world" (possibly more than once) then exit

A0.hello_world()

# Rubric:

# Submitted assignment through version control: 10 points
# Familiarity with version control is a valuable life skill for coders
 
# Contract: 10 points
# Good documentation can help make it easier to remember what code does

# Return value: 10 points
# Keeping track of return values will be much more important latter on
# This is calculated as the following:
def return_value_score():
	if A0.hello_world():
		return 10
	else:
		return 0

# Printing "hello world": 70 points
# This is the point of the assignment after all!