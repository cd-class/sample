## Name: Hello World
## In:  <nothing>
## Out: True if the function completes successfully, false otherwise
## prints the text "hello world"
## Example:
## >>> A0.hello_world() 
## hello world
## True
## >>>
def hello_world():
	print("hello world")
	return True