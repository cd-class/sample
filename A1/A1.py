# You may want to use helper functions for this assignment (or you may not), for example

## Name: helper
## In:  <something that needs to be processed in someway>
## Out: <the processed thing>
## <does something that takes a bit of code that would have to be written more than once>
def helper():
	# Your code here!
	return

## Name: analyze_file
## In:  input, a string such as "sample.in", and output, a string such as "sample.out" to be written to
##  - input should be a file that contains natural numbers separated by commas and nothing else
##  - there may be any number of lines
##  - lines may be of any length
## Out: 
## analyze_file should, for each line in input, write the sum of the numbers in the line, how many numbers were in the line, 
##     and the mean of the line rounded to the nearest natural number
## Example:  (this is how sample.out was generated)
## >>> A1.analyze_file("sample.in", "sample.out") 
## True
## >>>
def analyze_file(input, output):
	# Your code here!
		