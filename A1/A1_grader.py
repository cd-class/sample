import A1 as A1

# When run at command line as "python A0_grader", should print "hello world" then exit

A1.analyze_file("sample.in","sample.check")

# Rubric:

# Submitted assignment through version control: 10 points
 
# Contracts: 10 points

# Return values: 10 points
# This is calculated as the following:
def return_value_score():
	if A1.analyze_file("sample.in","sample.check"):
		return 10
	else:
		return 0

# Analyzing the file: 70 points
# This is calculated as the following:
def correctness_score():
	A1.analyze_file("sample.in","sample.check")
	check_values = open("sample.check","r").read().split(",")
	out_values = open("sample.out","r").read().split(",")
	score = 0 # just to start out!
	for i in len(check_values):	
		if (check_values[i] == out_values[i]):
			score = score + 1
	return (score / len(check_values)) * 70

