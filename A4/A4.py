# Buddhabrot

# s/o Melinda Green - https://en.wikipedia.org/wiki/Buddhabrot

# Check out the example mandelbrot code:
# https://github.com/cd-public/cd-public/blob/master/UNC-CH/Teaching/6_7/imager.py

# Imports - if you need these, use "pip install <name>"

from PIL import Image
import random
import numpy as np

# Name:  Create Base
# In:  size, the side length of a square two dimensional array in pixels
# Out:  a two dimensional array size by size of pixels that are each a three list of zeroes [0,0,0]
# Description/Example:  Create a base similar to "pixels" shown here 
# https://stackoverflow.com/questions/46923244/how-to-create-image-from-a-list-of-pixel-values-in-python3
# Let create_base(20)[5][10] represent the 10th element in the 5th row (where indices start at 0)
# create_base(2) = [[[0,0,0,],[0,0,0]],[[0,0,0,],[0,0,0]]]
def create_base(size):
	return # your code here
	
# Name:  Mandelbrot Sequence
# In:  two complex numbers a + bi as a complex(a,b), z_n and c 
# Out:  a complex number a + bi as a tuple (a,b)
# Description:  Calculate z_(n+1) = z_n^2 + c and return the result
# m_seq(complex(1,1),complex(0,0)) = 2j
def m_seq(z_n, c):
	return # your code here
	
# Name:  complex_to_base
# In:  a complex value c, a size
# Out:  a tuple (first,second) that gives the coordinates of the base pixel corresponding to the value c
# Description:  Figure out which pixel corresponds to a complex value
# Assume the base runs from [-2,2] in both real and complex values and c is in this space
# Assume positive real is down, positive imaginary is right
# Example c2b(complex(1,1), 2) = (1,1)
def c2b(c,size):
	return # your code here
	
# Name:  Escapes?
# In:  a complex value c, a number of iterations i
# Out:  a boolean
# Description:  Find out if complex value c "escapes" ie has absolute value greater than 2 w/i i iterations
# abs(z_n) > 2 means that the Mandelbrot sequence will not converge
# each iteration is one application of m_seq
# Example escapes(complex(1,1), 1000) = True 
def escapes(c, iter):
	return # your code here

# Name:  One Value Helper
# In:  A base, a number of iterations, a color in [0,1,2], a value c
# Out:  Base as updated
# Description (example infeasible, so detailed):
# Take a value c that is known to escape.
# Go through the escaping sequence and increment
# the pixel value for each location passed through by the 
# sequence for the given color. 
# I use (new value) = 255 - ((255 - (old value)) / (2+c))
# I increase color as I increase iterations so that 
# when I'm doing more incrementations because I have more
# iterations to incrememnt across, I don't get too bright too quickly
# 255 is the maximum brightness for a color in a pixel
def one_val_helper(base, iter, color, c):
	return # your code here
	
# Name:  One Value
# In:  A base, a list of iterations
# Out:  Base as updated
# Description (example infeasible, so detailed):
# Take a value.  Mirror the value across all axes.
# The value will be randomly generated
# For the value and it's mirrors, check to see if the value
# escapes for each of the number of iterations in iter
# If it does, go through the escaping sequence and increment
# the pixel value for each location passed through by the 
# sequence for the given color.  
# ie if rand1 = 1, rand2 = 2, check 1+2i, 2+i, -1+2i, -2+i, 1-2i, 2-i, -1-2i, -2-i,
def one_val(base, iters):
	return # your code here

# Name:  Buddhabrot
# In:  size (square size of the output image in pixels) iters (a list of length three of number of iterations)
# In, cont.:  vals, a base number of times to call one_val
# Out: nothing
# Description using one_val, Buddhabrot creates a an image by calling one_val many times
# on a create_base(size).  Once one_val has been called many times (vals/iter1)
# the image is saved using Image.fromarray and .save to "buddhabrot.png"
# The different iterations for different color bands are given as
# iters = [iter1, iter1*2, iter1*4]
# example:  buddhabrot(4001,[50,100,200],5000000) made buddhabrot.png
def buddhabrot(size, iters, vals):
	return # your code here
	
# buddhabrot(4001,[50,100,200],5000000)