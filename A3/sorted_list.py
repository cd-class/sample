# Assignment 3:  sorted_list.py

# Using the bst.py implemented provided and this template, create your own data structure:  a sorted list
# A sorted list is like a list in python but only contains integers and the integers are always in order

# Write the insert, size, contains, and remove functions given what you know from the provided code,
# from bst.py, and from online resources such as https://en.wikipedia.org/wiki/Linked_list

# Turn in sorted_list.py with the list implemented and I will grade it by testing different list operations on it.

class Node_SL:
	def __init__(self,initarg):
		self.data = initarg
		self.next = Empty()
		
	def __repr__(self):
		return "(" + str(self.data) + ")->" + self.next.__repr__()
	def __str__(self):
		return "(" + str(self.data) + ")->" + self.next.__repr__()
		
	def insert(self,arg):
		# your code here
		
	def size(self):
		# your code here
	
	def contains(self, arg):
		# your code here
				
	def remove(self, arg):
		# your code here
			
class Empty_SL:
	   
	def __repr__(self):
		return "()"
	def __str__(self):
		return "()"
		
	def insert(self,arg):
		# your code here
		
	def size(self):
		# your code here
	
	def contains(self, arg):
		# your code here
				
	def remove(self, arg):
		# your code here
		
# Name: new
# In:  Nothing
# Out:  An empty integer sorted list that supports any of the following:
#  * insert - takes an integer and returns the tree with the integer added
#  * size - returns the number of elements in a tree
#  * contains - takes an integer and returns True if the int is in the tree
#  * remove - takes an integer and returns a tree with the element not in it
# Description:  Implements an integer binary search tree
# Example:
# import sorted_list
# slist = sorted_list.new()
# for i in [5,3,7,2,4,6,8]:
#     slist = slist.insert(i)
# slist.contains(8) = True
# slist = slist.remove(5)
# slist.contains(5) = False
def new():
	# your code here