# From "https://www.dataquest.io/blog/making-538-plots/"

# How to Generate FiveThirtyEight Graphs in Python

# If you read data science articles, you may have already stumbled upon 
# FiveThirtyEight's content. Naturally, you were impressed by their 
# awesome visualizations. Using Python's matplotlib and pandas, we'll see that
# it's rather easy to replicate the core parts.

# We'll start here:
# https://www.dataquest.io/blog/content/images/2017/12/default_graph.png

# And end here:
# https://www.dataquest.io/blog/content/images/2017/12/final3.png

# Introducing the dataset

# We'll work with data describing the percentages of Bachelors conferred to
# women in the US from 1970 to 2011. We'll use a dataset compiled by data 
# scientist Randal Olson, who collected the data from the National Center 
# for Education Statistics.

# randalolson.com/wp-content/uploads/percent-bachelors-degrees-women-usa.csv

# To read in the .csv, we're going to use the pandas library
# To install pandas, I ran at commandline the follow:
# $ pip install pandas
# If that doesn't work for you, Google it or ask me.
# With that out of the way, we'll import pandas.

import pandas as pd

# Then we simple use the panda "read_csv" function

women_majors = pd.read_csv("percent-bachelors-degrees-women-usa.csv")

# If you want to see the python representation of the data, you can simply do
# > women_majors
# To see just snippets, try women_majors.head() for the first 5 rows
# For infor about the structure object, try women_majors.info()
# To see by running this file, include print, ie:

print("Welcome to graphing.py\n\n")
print("\n\n\nwomen_majors.tail() = \n\n")
print(women_majors.tail())

# Besides the Year column, every other column name indicates the subject of a 
# Bachelor degree. Every datapoint in the Bachelor columns represents the 
# percentage of Bachelor degrees conferred to women. Thus, every row describes 
# the percentage for various Bachelors conferred to women in a given year.

# Our Goal

# We now need to create a graph to help readers visualize the gender disparity 
# for Bachelors. We arbitrarily take a threshold of 20% in 1970 and now we want
# to graph the evolution for every such Bachelor.

# NOTE:  Consider, is this good selection criteria? (out of scope of this course)

# Let's first identify those specific Bachelors. We will use ".loc", to:
#  1 select the first row (the one that corresponds to 1970);
#  2 select the items in the first row only where the values are less than 20; 
#    the Year field will be checked as well, but not be included as 1970 > 20.
#  3 assign the resulting content to under_20.
under_20 = women_majors.loc[0, women_majors.loc[0] < 20]

print("\n\n\nunder_20 = \n\n")
print(under_20)

# Using matplotlib's default style

# To read in the .csv, we're going to use a function pandas library
# To install Pandas, I ran at commandline the follow:
# $ pip install pandas
# If that doesn't work for you, Google it or ask me.
# With that out of the way, we'll import matplotlib.

import matplotlib as mpl

# Let's take a look at a basic graph.

# Plot the graph using plot() on women_majors. We pass these parameters:
#  - "x" specifies the column from women_majors to use for the x-axis;
#  - "y" specifies the columns from women_majors to use for the y-axis; we'll 
#    use the index labels of under_20 from under20.index;
#  - "figsize" sets the size as a tuple (width, height) in inches.
# Assign the plot object to a variable named under_20_graph.

under_20_graph = women_majors.plot(x="Year", y=under_20.index, figsize=(12,8))

# To see that something is really there, let's check the type

print("\n\n\nprint(type(under_20_graph)) =\n\n")
print(type(under_20_graph))

# Then we'll save it to open with the image editor of our choice.
# The save operation works by generating a "figure" from a "plot"
# Then the plot is saved to a file.  Remember your file extension!

# You can uncomment this to generate the image:
# under_20_graph.get_figure().savefig("graph.png")

# I like to not have as much whitespace around the graph, so I use:

# You can uncomment this to generate the image:
# under_20_graph.get_figure().savefig(fname = "graph.png" bbox_inches="tight")

# Feel free to save and look at your graph any time throughout in this way.

# Take a look at the graph and compare it to the starting graph above.

# Using matplotlib's "fivethirtyeight" style

# The graph above has certain characteristics, like the width and color of the 
# spines, the font size of the y-axis label, the absence of a grid, etc. All 
# of these characteristics make up matplotlib's default style.

# As a short parenthesis, there's a few technical terms about the parts of a 
# graph throughout. You can refer to the legend below.

# https://www.dataquest.io/blog/content/images/2017/12/anatomy1.png

# Besides the default style, matplotlib comes with several built-in styles that
# we can use readily. To begin, we import matplotlib.style.

import matplotlib.style as style

# To see available styles, we can call style.available.

print("\n\n\nstyle available = \n\n")
print(style.available)

# You might have already observed that there's a built-in style called 
# fivethirtyeight. Let's use this style, and see where that leads. For that, 
# we'll use the aptly named use() function from the same matplotlib.style
# module (which we imported under the name style). Then we'll generate our 
# graph using the same code as earlier and save it again.

style.use('fivethirtyeight')
women_majors.plot(x = "Year", y = under_20.index, figsize = (12,8))

# Wow, that's a major change! With respect to our first graph, we can see that
# this one has a different background color, it has grid lines, there are no 
# spines whatsoever, the font size of the major tick labels are different, etc.

# The limitations of matplotlib's fivethirtyeight style

# All in all, using the fivethirtyeight style clearly brings us much closer. 
# Nonetheless, there's still a lot left to do. Let's examine a simple FTE graph 
# and see what else we need to add to our graph.

# https://www.dataquest.io/blog/content/images/2017/12/fandango.png

# Comparing the above graph with what we've made so far, we still need to:

#  - Add a title and a subtitle.
#  - Remove the block-style legend, and add labels near the relevant plot lines. 
#    We'll also have to make the grid lines transparent around these labels.
#  - Add a signature bottom bar which mentions the author and source.
#  - Add a couple of other small adjustments:
#     * increase the font size of the tick labels;
#     * add a "%" symbol to one of the major tick labels of the y-axis;
#     * remove the x-axis label;
#     * bold the horizontal grid line at y = 0;
#     * add an extra grid line next to the tick labels of the y-axis;
#     * increase the lateral margins of the figure.

# See an illustration here:

# https://www.dataquest.io/blog/content/images/2017/12/adjustments.png

# To minimize the time spent with generating the graph, it's important to avoid
# beginning adding the title, the subtitle, or any other text snippet. In 
# matplotlib, a text snippet is positioned by specifying the x and y 
# coordinates, as we'll see in some of the sections below. To replicate in 
# detail the FTE graph above, notice that we'll have to align vertically the 
# tick labels of the y-axis with the title and the subtitle. We want to avoid a
# situation where we have the vertical alignment we want, lost it by increasing
# the font size of the tick labels, and then have to change the position of the 
# title and subtitle again.

# Customizing the tick labels

# We'll start by increasing the font size of the tick labels. We plot the graph
# using the same code as earlier, and assign the resulting object to fte_graph. 
# Assigning to a variable allows us to repeatedly and easily apply methods on 
# the object, or access its attributes.
# We then increase the font size of all the major tick labels using the 
# tick_params() method with the following parameters:
#  - "axis" - specifies the axis to the tick labels we want to modify
#    (here we want to modify the tick labels of both axes)
#  - "which" - indicates what tick labels to be affected
#    (the major or the minor ones; see the legend shown earlier)
#  - "labelsize" - sets the font size of the tick labels.

fte_graph = women_majors.plot(x = "Year", y = under_20.index, figsize = (12,8))
fte_graph.tick_params(axis = "both", which = "major", labelsize = 18)

# You may have noticed that we didn't use style.use("fivethirtyeight") again.
# That's because the preference for any matplotlib style becomes global once 
# it's first declared in our code. We've set the style earlier as 
# fivethirtyeight, and from there on all subsequent graphs inherit this style.
# To return to the default state, just run style.use('default').

# We'll now build upon our previous changes by making a few adjustments:
#  - We add a "%" symbol to 50, the highest visible tick label of the y-axis.
#  - We also add a few whitespace characters after the other visible labels 
#    (to align them elegantly with the new "50%" label.)

# To make these changes to the tick labels of the y-axis, we'll use the 
# set_yticklabels() method along with the label parameter. As you can deduce
#  from the code below, this parameter can take in a list of mixed data types, 
# and doesn't require any fixed number of labels to be passed in.

# Customizing the tick labels of the y-axis
labels = [-10, "0   ", "10   ", "20   ", "30   ", "40   ", "50%"]
fte_graph.set_yticklabels(labels)


# You can check the labels as follows:
print("\n\n\nThe tick labels of the y-axis:", fte_graph.get_yticks(), "\n\n") 
# -10 and 60 are not visible on the graph

# Bolding the horizontal line at y = 0

# We will now bold the horizontal line where the y-coordinate is 0. For that,
# we'll use the axhline() method to add a new horizontal grid line, and cover
# the existing one. The parameters we use for axhline() are:

#  - "y" - specifies the y-coordinate of the horizontal line;
#  - "color" - indicates the color of the line;
#  - "linewidth" - sets the width of the line;
#  - "alpha" - regulates the transparency of the line

# Here we use "alpha" to regulate the intensity of the black color; the values 
# for alpha range from 0 (completely transparent) to 1 (completely opaque).

# Generate a bolded horizontal line at y = 0 
fte_graph.axhline(y = 0, color = "black", linewidth = 1.3, alpha = .7)

# Add an extra vertical line

# As we mentioned earlier, we have to add another vertical grid line in the 
# immediate vicinity of the tick labels of the y-axis. For that, we simply
# tweak the range of the values of the x-axis. Increasing the range's left
# limit will result in the extra vertical grid line we want.

# Add an extra vertical line by tweaking the range of the x-axis
fte_graph.set_xlim(left = 1969, right = 2011)

# Generating a signature bar

# The signature bar of the example has a few obvious characteristics:

#  - It's positioned at the bottom of the graph
#  - The author's name is located on the left side of the signature bar
#  - The source of the data is mentioned on the right side of the signature bar
#  - The text has a light grey color and a dark grey background

# It may seem difficult to add such a signature bar, but with a little 
# ingenuity we can get it done quite easily.

# We'll add a single snippet of text, give it a light grey color, and a
# background color of dark grey. We'll write both the author's name and the 
# source in a single text snippet, but we'll space out these two such that one
# ends up on the far left side, and the other on the far right. The nice thing
# is that the whitespace characters will get a dark grey background as well,
# which will create the effect of a signature bar.

# We'll also use some white space characters to align the author's name and the
# name of the source, as you'll be able to see in the next code block.

# This is also a good moment to remove the label of the x-axis. This way, we
# can get a better visual sense of how the signature bar fits in the overall
# scheme of the graph. 

# All told, we will:

#  - Remove the label of the x-axis by passing in a "False" value to the 
#    set_visible() method we apply to the object fte_graph.xaxis.label. 

# Think of it this way: we access the xaxis attribute of fte_graph, and then we
# access the label attribute of fte_graph.xaxis. 
# Then we finally apply set_visible() to fte_graph.xaxis.label.

#  - Add a snippet of text on the graph in the way discussed above. 
#    We'll use the text() method with the following parameters:
#     * "x" - specifies the x-coordinate of the text;
#     * "y" - specifies the y-coordinate of the text;
#     * "s" - indicates the text to be added;
#     * "fontsize" - sets the size of the text;
#     * "color" - specifies the color of the text; 

# The format of the value we use below is hexadecimal; we use this format to
# match exactly the background color of the entire graph

#     * "backgroundcolor" - sets the background color of the text snippet.

# Remove the label of the x-axis
fte_graph.xaxis.label.set_visible(False)

# The signature bar
x = 1965.8
y = -7
source = "Source: National Center for Education Statistics"
s = " " * 3 + "COMP 115" + " " * 83 + source + " " * 7
fs = 14
c = "#f0f0f0"
bc = "grey"
fte_graph.text(x=x, y=y, s=s, fontsize=fs, color=c, backgroundcolor=bc)
print("\n\n\nAdding signature bar with following text:\n\n" + s)

# Remember, to make the graph: 
# fte_graph.get_figure().savefig(fname = "graph.png", bbox_inches='tight')

# The x and y coordinates of the text snippet added were found through a
# process of trial and error. You can pass in decimals as the x and y
# parameters, so you'll be able to control the position of the text with a high
# level of precision.

# It's also worth mentioning that we tweaked the positioning of the signature
# bar in such a way that we added some visually refreshing lateral margins (we
# discussed this adjustment earlier). To increase the left margin, we simply
# lowered the value of the x-coordinate. To increase the right one, we added
# more whitespace characters between the author's name and the source's name - 
# this pushes the source's name to the right, resulting in the desired margin.

# A different kind of signature bar

# You'll also meet a slightly different kind of signature bar:

# https://www.dataquest.io/blog/content/images/2017/12/olympics1.png

# This kind of signature bar can be replicated quite easily as well. 
# We'll just add some grey colored text, and a line right above it.

# We'll create the visual effect of a line by adding a snippet of text of 
# multiple underscore characters ("_"). You might wonder why we're not using
# axhline() to simply draw a horizontal line at the y-coordinate we want. We
#  don't do that because the new line will drag down the entire grid of the
# graph, and this won't create the desired effect.

# We could also try adding an arrow, and then remove the pointer so we end up 
# with a line. However, the "underscore" solution is much simpler.

# The other signature bar
fte_graph.text(x = 1967.1, y = -6.5,
    s = "_" * 113,
    color = "grey", alpha = .7)

# "s" was defined for the previous signature bar
fte_graph.text(x = 1966.1, y = -9,
    s = s,
    fontsize = 14, color = "grey", alpha = .7)

# Adding a title and subtitle

# Add a title and a subtitle by using the same text() method we used to add
# text in the signature bar. If you already have some experience with
# matplotlib, you might wonder why we don't use the title() and suptitle()
# methods. This is because these two methods have an awful functionality with
# regard to moving text with precision. The only new parameter for text() is
# weight. We use it to bold the title.

# Adding a title and a subtitle
fte_graph.text(x = 1966.65, y = 62.7, 
	s = "The gender gap is transitory - even for extreme cases",
    fontsize = 26, weight = "bold", alpha = .75)
fte_graph.text(x = 1966.65, y = 57, 
    s = "Percentage of Bachelors conferred to women from 1970 to 2011 in the" +
	" US for\nextreme cases where the percentage was less than 20% in 1970",
    fontsize = 19, alpha = .85)

# NOTE:  Consider, is this title neutral? (out of scope of this course)

# In case you were wondering, the font used in the original FTE graphs is
# Decima Mono, a paywalled font. For this reason, we'll stick with Matplotlib's
# default font, which is similar.

# Adding colorblind-friendly colors

# Right now, we have that clunky, rectangular legend. We'll get rid of it, and
# add colored labels near each plot line. Each line will have a certain color,
# and a word of an identical color will name the Bachelor which that line
# corresponds to.

# First, however, we'll modify the default colors of the plot lines,
# and add colorblind-friendly colors:

# https://www.dataquest.io/blog/content/images/2017/12/cb_friendly.jpg

# We'll compile a list of RGB parameters for colorblind-friendly colors by
# using values from the above image. As a side note, we avoid using yellow
# because text snippets with that color are not easily readable on the graph's
# dark grey background color.

# After compiling this list of RGB parameters, we'll then pass it to the color
# parameter of the plot() method we used in our previous code. Note that
# matplotlib will require the RGB parameters to be within a 0-1 range, so we'll
# divide every value by 255, the maximum RGB value. We won't bother dividing
# the zeros because 0/255 = 0.

# Colorblind-friendly colors
colors = [[0,0,0], [230/255,159/255,0], [86/255,180/255,233/255],
          [0,158/255,115/255], [213/255,94/255,0], [0,114/255,178/255]]
		  
print("\n\n\nUsing colors:\n\n", colors, "\n\n")

# Make a graph with new colors
fte_graph = women_majors.plot(x = "Year", y = under_20.index, 
                              figsize = (12,8), color = colors)
							  
# You'll need to redo our other work on the new graph as well.
fte_graph.tick_params(axis = "both", which = "major", labelsize = 18)
labels = [-10, "0   ", "10   ", "20   ", "30   ", "40   ", "50%"]
fte_graph.set_yticklabels(labels)
fte_graph.axhline(y = 0, color = "black", linewidth = 1.3, alpha = .7)
fte_graph.set_xlim(left = 1969, right = 2011)
fte_graph.xaxis.label.set_visible(False)
x = 1965.8
y = -7
source = "Source: National Center for Education Statistics"
s = " " * 3 + "COMP 115" + " " * 83 + source + " " * 7
fs = 14
c = "#f0f0f0"
bc = "grey"
fte_graph.text(x=x, y=y, s=s, fontsize=fs, color=c, backgroundcolor=bc)
fte_graph.text(x = 1966.65, y = 62.7, 
	s = "The gender gap is transitory - even for extreme cases",
    fontsize = 26, weight = "bold", alpha = .75)
fte_graph.text(x = 1966.65, y = 57, 
    s = "Percentage of Bachelors conferred to women from 1970 to 2011 in the" +
	" US for\nextreme cases where the percentage was less than 20% in 1970",
    fontsize = 19, alpha = .85)

# Changing the legend style by adding colored labels

# Finally, we add colored labels to each plot line by using the text() method
# used earlier. The only new parameter is rotation, which we use to rotate each
# label so that it fits elegantly on the graph.

# We'll also do a little trick here, and make the grid lines transparent around
# labels by simply modifying their background color to match that of the graph.

# In our previous code we only modify the plot() method by setting the legend
# parameter to False. This will get us rid of the default legend. We also skip
# redeclaring the colors list since it's already stored in memory from the
# previous cell.

# The previous code we modify
fte_graph = women_majors.plot(x = "Year", y = under_20.index, figsize = (12,8),
                              color = colors, legend = False)
							  
# You'll need to redo our other work on the new graph as well.
fte_graph.tick_params(axis = "both", which = "major", labelsize = 18)
labels = [-10, "0   ", "10   ", "20   ", "30   ", "40   ", "50%"]
fte_graph.set_yticklabels(labels)
fte_graph.axhline(y = 0, color = "black", linewidth = 1.3, alpha = .7)
fte_graph.set_xlim(left = 1969, right = 2011)
fte_graph.xaxis.label.set_visible(False)
x = 1965.8
y = -7
source = "Source: National Center for Education Statistics"
s = " " * 3 + "COMP 115" + " " * 83 + source + " " * 7
fs = 14
c = "#f0f0f0"
bc = "grey"
fte_graph.text(x=x, y=y, s=s, fontsize=fs, color=c, backgroundcolor=bc)
fte_graph.text(x = 1966.65, y = 62.7, 
	s = "The gender gap is transitory - even for extreme cases",
    fontsize = 26, weight = "bold", alpha = .75)
fte_graph.text(x = 1966.65, y = 57, 
    s = "Percentage of Bachelors conferred to women from 1970 to 2011 in the" +
	" US for\nextreme cases where the percentage was less than 20% in 1970",
    fontsize = 19, alpha = .85)

# Add colored labels
xs = [1994, 1985, 2004, 2001, 1987]
ys = [44, 42.2, 51, 30, 11.5, 25]
bs = ["Agriculture",
      "Architecture",
      "Business",
      "Computer Science",
      "Engineering",
      "Physical Sciences"]
rs = [33, 18, -5, -42.5, 0, 27]
w = "bold"
bc = "#f0f0f0"
for i in range(5):
	fte_graph.text(x = xs[i], y = ys[i], s = bs[i], color = colors[0],
                   weight = w, rotation = rs[i], backgroundcolor = bc)
	print("Adding \"" + bs[i] + "\" label...\n")
	

# Just gonna save the final image here...
fte_graph.get_figure().savefig(fname = "graph.png", bbox_inches="tight")
print("\n\n\ngraph.png generated and saved")
				   
# Next steps

# That's it, our graph is now ready for publication!

# To do a short recap, we've started with generating a graph with matplotlib's
# default style. We then brought that graph to "FTE-level" as follows:

#  - We used matplotlib's in-built fivethirtyeight style.
#  - We added a title and a subtitle, and customized each.
#  - We added a signature bar.
#  - We removed the default legend, and added colored labels.
#  - We made a series of other small adjustments:
#     * customizing the tick labels,
#     * bolding the horizontal line at y = 0,
#     * adding a vertical grid line near the tick labels,
#     * removing the label of the x-axis, and
#     * increasing the lateral margins of the y-axis.

# To build upon what you've learned, here are a few next steps to consider:

# Generate a similar graph for other Bachelors.
# Generate different kinds of FTE graphs: a histogram, a scatter plot etc.
# Explore matplotlib's gallery to search for potential elements to enrich your
# FTE graphs (like inserting images, or adding arrows etc.). Adding images can
# take your FTE graphs to a whole new level:

# https://www.dataquest.io/blog/content/images/2017/12/dinos1.png