# A2_feedback.txt

# Rubric:

#  - Submit over git.
#  - Get matplotlib installed (pandas not required but recommended).
#  - Read in data from a file.
#  - Create any graph at all.
#  - Specify a matplotlib style.
#  - Add a title and a subtitle.
#  - Remove the block-style legend and add labels near the relevant plot lines. 
#  - Add a signature bottom bar which mentions the author and source.
#    Add a couple of other small adjustments:
#     * increase the font size of the tick labels;
#     * add a "%" symbol or appropriate label to a major labels of the y-axis;
#     * remove the x-axis label;
#     * bold the horizontal grid line at y = 0;
#     * add an extra grid line next to the tick labels of the y-axis;
#     * increase the lateral margins of the figure.
	
# There are 8 major objects (denoted "-") and 6 minor objectives (denoted "*").

# Major objectives will be worth 8 points.

# Minor objectives worth 6 points.

# GRADE : 100/100

# Comments: <passed eye test>

# Good contracts, good code, good job.  Keep it up!

# Code to commit this feedback
import os

os.system("git add A2_feedback.py")
os.system("git commit A2_feedback.py -m \"A2 feedback\"")
os.system("git push")