import pandas as pd, matplotlib as mpl, matplotlib.style as style
women_majors = pd.read_csv("percent-bachelors-degrees-women-usa.csv")
under_20 = women_majors.loc[0, women_majors.loc[0] < 20]
style.use("fivethirtyeight")
colors = [[0,0,0], [230/255,159/255,0], [86/255,180/255,233/255],
          [0,158/255,115/255], [213/255,94/255,0], [0,114/255,178/255]]
fte_graph = women_majors.plot(x = "Year", y = under_20.index, figsize = (12,8),
                              color = colors, legend = False)
fte_graph.tick_params(axis = "both", which = "major", labelsize = 18)
labels = [-10, "0   ", "10   ", "20   ", "30   ", "40   ", "50%"]
fte_graph.set_yticklabels(labels)
fte_graph.axhline(y = 0, color = "black", linewidth = 1.3, alpha = .7)
fte_graph.set_xlim(left = 1969, right = 2011)
fte_graph.xaxis.label.set_visible(False)
x = 1965.8
y = -7
source = "Source: National Center for Education Statistics"
s = " " * 3 + "COMP 115" + " " * 83 + source + " " * 7
fs = 14
c = "#f0f0f0"
bc = "grey"
fte_graph.text(x=x, y=y, s=s, fontsize=fs, color=c, backgroundcolor=bc)
fte_graph.text(x = 1966.65, y = 62.7, 
	s = "The gender gap is transitory - even for extreme cases",
    fontsize = 26, weight = "bold", alpha = .75)
fte_graph.text(x = 1966.65, y = 57, 
    s = "Percentage of Bachelors conferred to women from 1970 to 2011 in the" +
	" US for\nextreme cases where the percentage was less than 20% in 1970",
    fontsize = 19, alpha = .85)
xs = [1994, 1985, 2004, 2001, 1987]
ys = [44, 42.2, 51, 30, 11.5, 25]
bs = ["Agriculture",
      "Architecture",
      "Business",
      "Computer Science",
      "Engineering",
      "Physical Sciences"]
rs = [33, 18, -5, -42.5, 0, 27]
w = "bold"
bc = "#f0f0f0"
for i in range(5):
	fte_graph.text(x = xs[i], y = ys[i], s = bs[i], color = colors[0],
                   weight = w, rotation = rs[i], backgroundcolor = bc)
fte_graph.get_figure().savefig(fname = "graph.png", bbox_inches="tight")